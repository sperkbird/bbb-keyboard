#!/usr/bin/env ruby

# system & gem libs
require "fileutils"
require "logger"

# my libs
require "./lib/common/optparser"
require "./lib/physical/udpsender"
require "./lib/physical/gpiomon"
require "./lib/physical/gpiopin"

# static vars
APP_NAME = "hardwareConverter"
LOG_DIR = "./logs/"
LOG_FILE = "#{LOG_DIR}#{APP_NAME}.log"
LOG_LEVEL = Logger::INFO

GPIO_SETTING_DIR = Dir.pwd+"/gpio-settings/"

# Main Script

## logger setup
logger = nil
begin
  logger = Logger.new(LOG_FILE)
  logger.formatter = proc {| severity,datetime,progname,msg|
    "[#{severity}] #{datetime} #{msg}\n"
  }
  logger.level = LOG_LEVEL
rescue Errno::ENOENT => e
  begin
    FileUtils.mkdir_p("#{LOG_DIR}")
  rescue Exception => e
    puts "ERROR:LogFile can't create!"
    break_flag = true
  end
  unless break_flag
    retry
  end
end

## parse command line args
params = OptParser.new

## start up logs
logger.info "----------"
logger.info "#{APP_NAME} is started"
logger.info "==SETTINGS"
logger.info "SEND_PORT:#{params.getSendPort}"
logger.info "DAEMON_MODE:#{params.getDaemonFlag}"
logger.info "DEST_ADDRESS:#{params.getDstAddress}"
logger.info "GPIO_SETTING_DIR:#{GPIO_SETTING_DIR}"
logger.info "==SETTINGS END"


## create UdpSender
udpsender = UdpSender.new(params.getDstAddress,params.getSendPort,logger)

## create GpioMonitor
monitor = GpioMonitor.new(udpsender,logger,GPIO_SETTING_DIR)

## Main loop
begin
monitor.mon
rescue => e
  # Catch Fatal Error
  logger.fatal e.class
  logger.fatal e.message
ensure
  monitor.close # Close GPIOs
  logger.info "#{APP_NAME} Process Terminated"
end
