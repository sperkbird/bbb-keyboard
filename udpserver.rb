#!/usr/bin/env ruby

require "socket"

sock = UDPSocket.new

sock.bind("0.0.0.0",62226)
puts "server created by 0.0.0.0:62226"

loop do
  data = sock.recv(65535)
  data.chomp!
  p "data=>#{data}"
  File.open("./gamestate/value","w") do | file |
    file.puts data
  end
end
