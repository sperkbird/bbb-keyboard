require "beaglebone"
include Beaglebone
GPIO_PORT_NUM = "P8"
LIGHT_TIME =1

class GpioPin
  def initialize(input,output,interval)
    @input_pin  = GPIOPin.new("#{GPIO_PORT_NUM}_#{input}".to_sym, :IN)
    @output_pin = GPIOPin.new("#{GPIO_PORT_NUM}_#{output}".to_sym,:OUT)
    @cnt = 0
    @interval = interval
    if @input_pin.digital_read == :HIGH
      @state = 1
    else
      @state = 0
    end
    @thread = Thread.start {
      loop do
        if @input_pin.digital_read == :HIGH
          @state = 1
        else
          @state = 0
        end
        if @cnt > 0
          @cnt -= 1
        end
        sleep(@interval)
      end
    }
  end

  def getValue
    return @state
  end

  def getCnt
    return @cnt
  end

  def setCnt(cnt)
    @cnt = cnt
  end

  def setLed(time=LIGHT_TIME)
    Thread.start {
      @output_pin.digital_write(:HIGH)
      sleep(time)
      @output_pin.digital_write(:LOW)
    }
  end
 
  def close
    @thread.kill
    sleep(0.01)
    @input_pin.disable_gpio_pin
    @input_pin = nil
    @output_pin.disable_gpio_pin
    @output_pin = nil
  end
end
