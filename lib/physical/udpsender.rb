require "socket"

class UdpSender
  def initialize(dst_add,port,logger)
    @logger = logger
    @send_port = port
    @dst_add = dst_add
    @logger.info "[#{self.class}] created"
    @sock = UDPSocket.new
    @sock.connect(@dst_add,@send_port)
  end

  def send(msg)
    @sock.send(msg.to_s,0)
  end

end
