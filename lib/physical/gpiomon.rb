#require "Thread"

class GpioMonitor
  GPIO_DIR = "/sys/class/gpio/"
  #GPIO_DIR = "./sys/class/gpio/" # For debug
  INTERVAL_SEC = 0.001
  LIGHT_TIME = 1

  def initialize(sender,logger,settingdir)
    @sender = sender
    @logger = logger
    @gamestate = false
    @logger.info "[#{self.class}] created"
    @input_data = Hash.new
    @btns = Hash.new
    settingGpio(settingdir)
  end
  
  def close
    for num in 0..14 do
      @btns[num].close
      @logger.info "[#{self.class}] Btn#{num} closed"
    end
    @logger.info "GPIOs Closed"
  end

  def mon
    @logger.info "[#{self.class}] Monitoring:#{GPIO_DIR}gpio*/*"
    loop do
      for num in 0..14 do
        state = @btns[num].getValue
        if @input_data[num][:val] != state
          @logger.debug "[#{self.class}] Btn#{num} Pushed"
          if @btns[num].getCnt == 0
            @btns[num].setLed
            @btns[num].setCnt(5)
            @sender.send(num)
          end
          @input_data[num][:val] = state
        end
      end
    end

  end

  # Private Methods
  private


  def settingGpio(settingdir)
    # setting Input GPIOs
    @input_map, @input_gpio_list = mapGpios(settingdir+"input.csv","input")
    # setting Output GPIOs
    @output_map = mapGpios(settingdir+"output.csv","output")

    #TODO Debug Code delete
    puts "input_map"
    p @input_map
    puts "output_map"
    p @output_map

    for num in 0..14 do
      @input_data[num] = Hash.new
      @btns.store(num,GpioPin.new(@input_map.key(num.to_s),@output_map[num.to_s],INTERVAL_SEC))
      @input_data[num][:val] = @btns[num].getValue
      @input_data[num][:time] = Time.now
    end
  end

  def mapGpios(file,type)
    # check file type
    unless type == "input" || type == "output"
      raise "Undefined file type"
    end

    map = Hash.new
    File.foreach(file) do |line|
      button,pin = line.chomp.split(",")
      if type == "input"
        map.store(pin.to_s,button.to_s)
      else
        map.store(button.to_s,pin.to_s)
      end
    end
    return map
  end

end
