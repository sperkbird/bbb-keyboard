require "fssm"

class GpioMonitor
  GPIO_DIR = "/sys/class/gpio/"
  #GPIO_DIR = "./sys/class/gpio/" # For debug
  INTERVAL_SEC = 1

  def initialize(sender,logger,settingdir)
    @sender = sender
    @logger = logger
    @gamestate = false
    @logger.info "[#{self.class}] created"
    @input_data = Hash.new
    settingGpio(settingdir)
  end
  
  def close
    def closeGpio(array)
      array.each do | value |
        File.open(GPIO_DIR+"unexport","a") do | file |
          file.puts(value)
        end
      end
    end
    closeGpio(@input_map.keys)
    closeGpio(@output_map.values)
    @logger.info "GPIOs Closed"
  end

  def mon
    @logger.info "[#{self.class}] Monitoring:#{GPIO_DIR}gpio*/*"

    def gpioProcess(base,file)
      @logger.info "hit button #{file}"
      if checkFileName(file)
        no = getGpioInput(base,file)
        #TODO ゲーム状態対応用
        unless no.nil? # and !@gamestate
          @udpsender.send(@input_map[no.to_s])
        end
      end
    end

    def gameProcess(base,file)
      File.eachline(base+file) do | line |
        if line.to_s == "1"
          @gamestate = false
          @logger.info "[#{self.class}] GameEnd!"
        elsif line.to_s == "0"
          @gamestate = true
          @logger.info "[#{self.class}] GameStart!"
        end
      end
    end

    FSSM.monitor GPIO_DIR,"gpio*/value"  do
        create do | base,file |
#          gpioProcess(base,file)
        end

        update do | base,file |
#          gpioProcess(base,file)
        end

        delete do | base,file |
          #Nothing to do
        end
    end
=begin
      path "./gamestate/","value" do
        create do | base,file |
          gameProcess(base,file)
        end

        update do | base,file |
          gameProcess(base,file)
        end
      end
=end
  end

  # Private Methods
  private

  def settingGpio(settingdir)
    # setting Input GPIOs
    @input_map, @input_gpio_list = exportGpios(settingdir+"input.csv","input")
    # setting Output GPIOs
    @output_map = exportGpios(settingdir+"output.csv","output")
    @output_map.values.each do | val |
      File.open(GPIO_DIR+"gpio#{val}/direction","w") do | file |
        file.puts "out"
      end
    end

    #TODO Debug Code delete
    puts "input_map"
    p @input_map
    puts "output_map"
    p @output_map
    puts "input_gpio_list"
    p @input_map.keys

    # get Default GPIO value
    @input_map.keys.each do | no |
      @input_data[no.to_s] = getGpioValue(GPIO_DIR+"gpio#{no}","value")
    end
    puts "inited input_data"
    p @input_data

  end

  def exportGpios(file,type)
    # check file type
    unless type == "input" || type == "output"
      raise "Undefined file type"
    end

    map = Hash.new
    File.foreach(file) do |line|
      button,pin = line.chomp.split(",")
      File.open(GPIO_DIR+"export","a") do | file |
        file.puts(pin)
        if type == "input"
          map.store(pin.to_s,button.to_s)
        else
          map.store(button.to_s,pin.to_s)
        end
      end
    end
    return map
  end

  # file change detect actions
  def checkFileName(file)
    result = false
    if file == "value"
      result = true
    end

    return result
  end

  def getGpioValue(base,file)
    result = nil
    gpio_no = base.split("/")[-1][5..-1].to_i

    File.foreach("#{base}/#{file}") do |line|
      result = line.chomp # 行末改行を削除
    end
    return result
  end

  def getGpioInput(base,file)
    result = nil
    gpio_no = base.split("/")[-1][5..-1].to_s
    value = getGpioValue(base,file)
    # 前回ボタンが押された時とON/OFFが違うときのみ
    if @input_data[gpio_no][:value] != value
      # 前回ボタンが押されてからINVERVAL_SECの秒数が経過していた時のみ
      if (Time.now + INTERVAL_SEC) <= val[:time]
        @input_data[gpio_no][:time] = Time.now
        Thread.start {
          @logger.debug "Pushed Button#{@input_map[gpio_no]}"
          button = @input_map[gpio_no]
          File.open("#{GPIO_DIR}gpio#{@output_map[button]}/value","w") do | file |
            file.puts "1"
          end
          sleep(LED_INTERVAL_TIME)
          File.open("#{GPIO_DIR}gpio#{@output_map[button]}/value","w") do | file |
            file.puts "0"
          end
        }
      end
      result = @input_data[gpio_no][:val] = value
    end

    return result
  end

# file change detect test methods
  def create_action(base,file)
    p "create #{file}:FULLPASS=#{base}/#{file}"
    File.foreach("#{base}/#{file}") do | f |
      p f.chomp
    end
  end

  def update_action(base,file)
    p "update #{file}:FULLPASS=#{base}/#{file}"
    File.foreach("#{base}/#{file}") do | f |
      p f.chomp
    end
  end

  def delete_action(base,file)
    p "delete #{file}:FULLPASS=#{base}/#{file}"
  end
end
