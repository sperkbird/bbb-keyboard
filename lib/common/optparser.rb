# This file is Ruby Project common library
# Command line args parse Class
# Beagle Bone Black GPIO KeyBoard Project
# Git repo: bitbucket.org:sperkbird/bbb-keyboard.git

# Copyright 2015 Kobedenshi.ac.jp Procon Club All Rights Reserved.
# Created by Kenta Kamihigoshi(sperkbird@gmail.com)

# For Command line args parse
require "optparse"
require "dotenv"
require "ipaddr"

class OptParser
  # DEFAULT PARAMS
  DAEMON = false
  SEND_PORT = 62226
  RECEIVE_PORT = 62226
  DST_ADD = "127.0.0.1"

  # Init Method
  def initialize(argv=ARGV)
    result = false
    message = nil
    begin
      @params = ARGV.getopts("d","daemon","p:","port:","destadd:")
      checkParams
    rescue => e
      p "Error: fail parse args"
      p e
      p @params
    end
  end

  # Public Method
  def getParam(paramname="params")
    return self.instance_variable_get("@#{paramname}")
  end

  def getDaemonFlag
    return @daemon_flag
  end

  def getSendPort
    return @send_port
  end

  def getReceivePort
    return @receive_port
  end

  def getDstAddress
    return @dst_add
  end

  # Private Methods
  private

  def checkParams
    @daemon_flag = checkDaemonParam
    @send_port = checkSendPortParam
    @receive_port = checkReceivePortParam
    @dst_add = checkDstAddParam
  end

  def checkDaemonParam
    result = ENV["DAEMON"] || DAEMON
    unless !@params["d"] and !@params["daemon"]
      result = true
    end

    return result
  end

  def checkSendPortParam
    result = ENV["SEND_PORT"] || SEND_PORT
    port = nil

    unless @params["p"].nil?
      port = @params["p"].to_i
    else
      unless @params["sport"].nil?
        port = @params["sport"].to_i
      end
    end

    if !port.nil? and port > 0
        result = port
    end

    return result
  end

  def checkReceivePortParam
    result = ENV["RECEIVE_PORT"] || RECEIVE_PORT
    port = nil

    unless @params["r"].nil?
      port = @params["r"].to_i
    else
      unless @params["rport"].nil?
        port = @params["rport"].to_i
      end
    end

    if !port.nil? and port > 0
      result = port
    end

    return result
  end

  def checkDstAddParam
    result = ENV["DST_ADD"] || DST_ADD
    address = nil

    unless @params["destadd"].nil?
      address = @params["destadd"].to_s
    end

    if !address.nil?
      begin
        addr = IPAddr.new(address)
        if addr.ipv4? or addr.ipv6?
          result = address
        end
      rescue
      end
    end

    return result
  end

end
